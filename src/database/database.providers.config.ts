import { Injectable } from "@nestjs/common";
import { TypeOrmOptionsFactory, TypeOrmModuleOptions } from "@nestjs/typeorm";

@Injectable()
export class TypeOrmConfigService implements TypeOrmOptionsFactory {
  
  createTypeOrmOptions(): TypeOrmModuleOptions {
    const postgresConnect:any = {
      type: 'postgres',
      host: process.env.DB_HOST,
      port: Number(process.env.DB_PORT),
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_DATABASE,
      timezone: process.env.DB_TIMEZONE,
      entities: [__dirname + '/../**/*.entity.{js,ts}'],
      synchronize: true,
      // dropSchema: true,
      // logging: true,
      // logger: "file"
    };
    console.log('postgresConnect========:: ',postgresConnect)
    return postgresConnect
  }
}