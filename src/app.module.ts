import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ServeStaticModule } from '@nestjs/serve-static';
import { TypeOrmModule } from '@nestjs/typeorm';
import { join } from 'path';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmConfigService } from './database/database.providers.config';
import { ProductsModule } from './modules/products/products.module';

@Module({
  imports: [ProductsModule,
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: `src/env/${process.env.ENV || 'dev'}.env`,
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useClass: TypeOrmConfigService,
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'files')
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
