import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    ManyToOne,
} from "typeorm";
import { ProductEntity } from "./product.entity";

@Entity('product_image')
export class ProductImageEntity {
    @PrimaryGeneratedColumn()
    id?: string;

    @Column({ type: 'varchar', nullable: true })
    image_name: string;

    @Column({ type: 'varchar', nullable: true })
    image_type: string;

    @Column({ type: 'varchar', nullable: true })
    image_url: string;

    @ManyToOne(() => ProductEntity, Column => Column.files)
    product: ProductEntity;

    @CreateDateColumn() createOn?: Date;
    @CreateDateColumn() updateOn?: Date;

}