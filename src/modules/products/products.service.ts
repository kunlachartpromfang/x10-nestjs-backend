
import { Injectable, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as fs from 'fs';
import { MESSAGES } from 'src/constansts/message';
import { ENV } from 'src/env/env';
import { IResponse } from 'src/interfaces/type-response';
import { gen_uuid } from 'src/shared';
import { returnResponse } from 'src/shared/return-response';
import { Repository } from 'typeorm';
import { CreateProductDto } from './dto/create-product.dto';
import { ProductImageEntity } from './entities/image.entity';
import { ProductEntity } from './entities/product.entity';
@Injectable()
export class ProductsService {

  constructor(
    @InjectRepository(ProductEntity)
    private readonly productRepo: Repository<ProductEntity>,
    @InjectRepository(ProductImageEntity)
    private readonly imageRepo: Repository<ProductImageEntity>,
  ) { }


  async create(files: Array<Express.Multer.File>, body: CreateProductDto): Promise<IResponse> {
    try {
      const productModel = new ProductEntity()
      productModel.p_name = body.p_name
      productModel.p_type = body.p_type
      productModel.p_color = body.p_color
      productModel.p_description = body.p_description
      productModel.p_price = body.p_price
      productModel.p_size = body.p_size
      productModel.p_status = body.p_status
      const saveProduct = await this.productRepo.save(productModel)
      if (saveProduct) {

        for (const item of files) {
          const img_path = `${gen_uuid().toString()}.${item.mimetype.split('/')[1]}`

          fs.writeFileSync(`./files/` + img_path, item.buffer)
          const productImageModel = new ProductImageEntity()
          productImageModel.image_name = gen_uuid().toString()
          productImageModel.image_type = item.mimetype.split('/')[1]
          productImageModel.image_url = ENV().URL_API + img_path
          productImageModel.product = saveProduct;
          await this.imageRepo.save(productImageModel)
        }



        return returnResponse(true, HttpStatus.CREATED, MESSAGES.SUCCESS)

      } else {
        return returnResponse(false, HttpStatus.BAD_REQUEST, MESSAGES.BAD_REQUEST)
      }

    } catch (error) {
      return returnResponse(true, HttpStatus.INTERNAL_SERVER_ERROR, error)
    }

  }



  async findAll(): Promise<IResponse> {
    try {
      const findAll = await this.productRepo.find({ relations: { files: true } })
      return returnResponse(true, HttpStatus.OK, MESSAGES.SUCCESS, findAll)

    } catch (error) {
      return returnResponse(true, HttpStatus.INTERNAL_SERVER_ERROR, error)
    }
  }

  async findOne(id: string): Promise<IResponse> {
    try {
      const findOne = await this.productRepo.findOne({ where: { id }, relations: { files: true } })
      return returnResponse(true, HttpStatus.OK, MESSAGES.SUCCESS, findOne)

    } catch (error) {
      return returnResponse(true, HttpStatus.INTERNAL_SERVER_ERROR, error)
    }
  }

  async update(id: string, body: CreateProductDto, file: Array<Express.Multer.File>,): Promise<IResponse> {
    try {
      const { files, ...result } = body
      console.log("body::", body)
      console.log("result::", file)
      const updateProduct = await this.productRepo.update(id, result)
      const product = await this.productRepo.findOne({ where: { id } })
      for (const item of file) {
        const img_path = `${gen_uuid().toString()}.${item.mimetype.split('/')[1]}`

        fs.writeFileSync(`./files/` + img_path, item.buffer)
        const productImageModel = new ProductImageEntity()
        productImageModel.image_name = item.originalname
        productImageModel.image_type = item.mimetype.split('/')[1]
        productImageModel.image_url = ENV().URL_API + img_path
        productImageModel.product = product;
        await this.imageRepo.save(productImageModel)
      }
      console.log(updateProduct)
      return returnResponse(true, HttpStatus.OK, MESSAGES.SUCCESS)

    } catch (error) {
      return returnResponse(true, HttpStatus.INTERNAL_SERVER_ERROR, error)
    }
  }

  async remove(id: string) {
    try {
      const image = await this.imageRepo.find({ where: { product: { id } } })
      if (image.length > 0) {
        image.forEach(async element => {
          await this.imageRepo.delete(element.id)
        })
      }
      await this.productRepo.delete(id)
      console.log(image)

      return returnResponse(true, HttpStatus.OK, MESSAGES.SUCCESS)

    } catch (error) {

      return returnResponse(true, HttpStatus.INTERNAL_SERVER_ERROR, error)

    }
  }
}
